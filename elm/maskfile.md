# Commands

## test

```sh
elm-test-rs
```

### test watch

```sh
elm-test-rs --watch
```

## lint

```sh
yes | elm-review --fix
```

## format

```sh
elm-format --yes src/ tests/
```

## update

```bash
set -o errexit
set -o nounset
set -o pipefail

nix flake update
direnv exec . \
    devbox update
direnv exec . \
    elm-json upgrade --unsafe --yes
pushd "./review"
direnv exec . \
    elm-json upgrade --unsafe --yes
```

---

<!-- markdownlint-disable-next-line MD039 MD045 -->
This folder has been setup from the [`nix-sandboxes`'s template ![](https://img.shields.io/gitlab/stars/pinage404/nix-sandboxes?style=social)](https://gitlab.com/pinage404/nix-sandboxes)
