# Nix Sandboxes

[![built with nix](https://builtwithnix.org/badge.svg)](https://builtwithnix.org)

Sandboxes / starters for differents languages

## Requirements

* Install [Git](https://git-scm.com)
* Install [Nix](https://nixos.org)
  * Preferably with [`nix-installer`](https://github.com/DeterminateSystems/nix-installer)
* Install [DirEnv](https://direnv.net)

## Usage

[![Open in Gitpod](https://gitpod.io/button/open-in-gitpod.svg)](https://gitpod.io/#https://gitlab.com/pinage404/nix-sandboxes)

### Automatic

#### List sandboxes

```sh
NIX_CONFIG='extra-experimental-features = flakes nix-command' \
nix run 'gitlab:pinage404/nix-sandboxes'
```

#### Get one sandbox

e.g. for Rust

```sh
NIX_CONFIG='extra-experimental-features = flakes nix-command' \
nix run 'gitlab:pinage404/nix-sandboxes' rust ./your_new_project_directory
```

### Manual

1. There is 2 possibles ways
    * **Lighten / Faster**
        * Take just one langage

          e.g. for Rust

          ```sh
          NIX_CONFIG="extra-experimental-features = flakes nix-command" \
          nix flake new --template "gitlab:pinage404/nix-sandboxes#rust" ./your_new_project_directory
          ```

        * Move to the directory

          ```sh
          cd ./your_new_project_directory
          ```

    * **Complete**
        * Clone this repository

          ```sh
          git clone https://gitlab.com/pinage404/nix-sandboxes.git
          cd nix-sandboxes
          ```

        * Move to the directory

          e.g. for Rust

          ```sh
          cd rust
          ```

1. Enable DirEnv

    This will trigger Nix to get and setup everything that is needed

    ```sh
    direnv allow
    ```

1. *Optional steps*
    * If you use VSCode
        * Install [VSCode](https://code.visualstudio.com)
        * Open from the command line in the folder

          ```sh
          code .
          ```

        * Install recommanded extensions

    * To avoid rebuilding things that have already been built elsewhere, use Cachix
        * Install [Cachix](https://www.cachix.org/)
        * Use Nix's cache

          ```sh
          cachix use pinage404-nix-sandboxes
          ```

    * If you use [`git-gamble`](https://gitlab.com/pinage404/git-gamble/-/blob/main/README.md) : a tool that blend TCR + TDD to make sure to **develop** the **right** thing, **baby step by baby step**
        * Install [`git-gamble`](https://gitlab.com/pinage404/git-gamble/-/blob/main/README.md#how-to-install)
            * If you cloned the repository, it's already included
            * If you used a template, in the `.envrc` uncomment `use flake gitlab:pinage404/git-gamble?rev=06f50aeb39227e0cc901978a14a27bb9a935c958&dir=/packaging/nix/flake/`

              ```diff
              -# use flake "gitlab:pinage404/git-gamble?rev=06f50aeb39227e0cc901978a14a27bb9a935c958&dir=/packaging/nix/flake/"
              +use flake "gitlab:pinage404/git-gamble?rev=06f50aeb39227e0cc901978a14a27bb9a935c958&dir=/packaging/nix/flake/"
              ```

        * Tests should pass

          ```sh
          git gamble --pass
          ```

1. Enjoy

## Usefull links

### Exercices / Katas

List of exercices / katas

* [Awesome Katas](https://github.com/gamontal/awesome-katas)
* [Kata Log](https://kata-log.rocks/index.html)
* [List of excercices](https://github.com/cyber-dojo/exercises-start-points/tree/master/start-points)
  * from [cyber-dojo](https://cyber-dojo.org/creator/home)
* [Samman Technical Coaching's Katas](https://sammancoaching.org/kata_descriptions/index.html)
* [Emily Bache's Katas](https://github.com/emilybache?tab=repositories)
* [Coding Dojo's Katas](https://codingdojo.org/kata/)
* [Xavier Nopre's Katas](https://github.com/xnopre/xnopre-katas)

### Help with languages

List of tools to help writing in some language

* [Programming-Idioms](https://programming-idioms.org/)
* [Syntax Cheatsheet for Javascript/Python](https://www.theyurig.com/blog/javascript-python-syntax)
* [Awesome Lists](https://awesome.digitalbunker.dev/) curated lists of links related to a topic
* [Hello, World! in differents languages on Wikipedia](https://en.wikipedia.org/wiki/%22Hello,_World!%22_program#Examples)
* [Rosetta Code](https://www.rosettacode.org) same programs in many languages
* [The Algorithms](https://the-algorithms.com) many algorithms in many languages

### Tool comparison table

|                                | Platform     | Package Manager   | Formater         | Linter                                     | Test Framework      |  Run  |
| :----------------------------- | :----------- | :---------------- | :--------------- | :----------------------------------------- | :------------------ | :---: |
| [C]                            | [Clang]      |                   | [clang-format]   |                                            | [Unity]             |   ✅   |
| [Clojure]                      | [jdk]        | [deps]            | [cljfmt]         | [clj-kondo], [kibit], [eastwood], [splint] | [clojure.test]      |   ✅   |
| [C++]                          | [Clang]      |                   | [clang-format]   |                                            | [Catch2]            |   ✅   |
| [Common Lisp]                  | [sbcl]       |                   | [lisp-format]    |                                            | [FiveAM]            |   ✅   |
| [Elm]                          | [NodeJS]     | [elm-json]        | [elm-format]     | [elm-review]                               | [elm-test-rs]       |   ❌   |
| [Erlang]                       |              | [rebar3]          | [erlfmt]         |                                            | [EUnit]             |   ✅   |
| [Forth]                        | [gforth]     |                   |                  |                                            | [ffl]               |   ❌   |
| [F#]                           | [DotNet]     | [DotNet]          | [Fantomas]       | [FSharpLint]                               | [xUnit]             |   ❌   |
| [Haskell]                      | [GHC]        | [Cabal]           | [Fourmolu]       | [HLint]                                    | [Hspec]             |   ✅   |
| [Nix]                          |              |                   | [nixpkgs-fmt]    | [statix], [nix-linter], [deadnix]          | [Nixpkgs]           |   ❌   |
| [PHP]                          |              | [Composer]        |                  |                                            | [PHPUnit]           |   ✅   |
| [Prolog]                       | [SWI-Prolog] |                   |                  |                                            | [Prolog Unit Tests] |   ✅   |
| [Python]                       |              | [Poetry]          | [Black], [isort] | [Flake8], [mypy]                           | [pytest]            |   ✅   |
| [Roc]                          |              |                   | [Roc]            |                                            | [Roc]               |   ❌   |
| [Ruby]                         |              |                   |                  |                                            | [RSpec]             |   ❌   |
| [Rust]                         |              | [rustup], [Cargo] | [Rustfmt]        |                                            | [speculoos]         |   ✅   |
| [Shell]                        | [Bash]       |                   | [shfmt]          | [ShellCheck]                               | [shUnit2]           |   ✅   |
| [Snapshot Testing]             |              |                   |                  |                                            | [delta]             |   ✅   |
| [TypeScript] [Bun]             | [Bun]        | [Bun]             | [Bun]            |                                            | [Bun]               |   ✅   |
| [TypeScript] [Deno]            | [Deno]       | [Deno]            | [Deno]           | [Deno]                                     | [Deno]              |   ✅   |
| [TypeScript] [NodeJS] [Jest]   | [ts-node]    | [NPM]             | [Prettier]       |                                            | [Jest]              |   ✅   |
| [TypeScript] [NodeJS] [Vitest] | [vite-node]  | [NPM]             | [Prettier]       |                                            | [Vitest]            |   ✅   |

[C]: https://en.wikipedia.org/wiki/C_%28programming_language%29
[Clang]: https://clang.llvm.org
[clang-format]: https://clang.llvm.org/docs/ClangFormat.html
[Unity]: https://github.com/ThrowTheSwitch/Unity/

[Clojure]: https://clojure.org
[jdk]: https://openjdk.java.net
[deps]: https://clojure.org/guides/deps_and_cli
[cljfmt]: https://github.com/weavejester/cljfmt
[clj-kondo]: https://github.com/clj-kondo/clj-kondo
[kibit]: https://github.com/clj-commons/kibit
[eastwood]: https://github.com/jonase/eastwood
[splint]: https://github.com/noahtheduke/splint
[clojure.test]: https://clojuredocs.org/clojure.test

[Common Lisp]: https://lisp-lang.org
[sbcl]: http://www.sbcl.org
[lisp-format]: https://github.com/eschulte/lisp-format
[FiveAM]: https://common-lisp-libraries.readthedocs.io/fiveam/

[C++]: https://en.cppreference.com/w/
[Clang]: https://clang.llvm.org
[clang-format]: https://clang.llvm.org/docs/ClangFormat.html
[Catch2]: https://github.com/catchorg/Catch2

[Elm]: https://elm-lang.org
[NodeJS]: https://nodejs.org
[elm-json]: https://github.com/zwilias/elm-json
[elm-format]: https://github.com/avh4/elm-format
[elm-review]: https://github.com/jfmengels/elm-review
[elm-test-rs]: https://github.com/mpizenberg/elm-test-rs

[Erlang]: https://www.erlang.org
[rebar3]: https://rebar3.org
[erlfmt]: https://github.com/WhatsApp/erlfmt
[EUnit]: https://www.erlang.org/doc/apps/eunit/chapter.html

[Forth]: https://forth-standard.org
[gforth]: https://github.com/forthy42/gforth
[ffl]: https://irdvo.nl/FFL/

[F#]: https://fsharp.org
[DotNet]: https://dotnet.microsoft.com/en-us/
[Fantomas]: https://fsprojects.github.io/fantomas/
[FSharpLint]: https://fsprojects.github.io/FSharpLint/
[xUnit]: https://xunit.net

[Haskell]: https://www.haskell.org
[GHC]: https://www.haskell.org/ghc/
[Cabal]: https://www.haskell.org/cabal/
[Fourmolu]: https://fourmolu.github.io/
[HLint]: https://github.com/ndmitchell/hlint
[Hspec]: https://hspec.github.io

[Nix]: https://nixos.org/manual/nix/stable/
[nixpkgs-fmt]: https://nix-community.github.io/nixpkgs-fmt/
[statix]: https://git.peppe.rs/languages/statix/about/
[nix-linter]: https://github.com/Synthetica9/nix-linter
[deadnix]: https://github.com/astro/deadnix
[Nixpkgs]: https://nixos.org/manual/nixpkgs/stable/#sec-functions-library-debug

[PHP]: https://www.php.net
[Composer]: https://getcomposer.org
[PHPUnit]: https://phpunit.de

[Prolog]: https://en.wikipedia.org/wiki/Prolog
[SWI-Prolog]: https://www.swi-prolog.org
[Prolog Unit Tests]: https://www.swi-prolog.org/pldoc/doc_for?object=section%28%27packages/plunit.html%27%29

[Python]: https://www.python.org
[Poetry]: https://python-poetry.org
[Black]: https://black.readthedocs.io/en/stable/
[isort]: https://pycqa.github.io/isort/
[Flake8]: https://flake8.pycqa.org
[mypy]: https://www.mypy-lang.org
[pytest]: https://pytest.org

[Roc]: https://roc-lang.org

[Ruby]: https://www.ruby-lang.org
[RSpec]: https://rspec.info

[Rust]: https://www.rust-lang.org
[rustup]: https://rustup.rs
[Cargo]: https://doc.rust-lang.org/cargo/
[Rustfmt]: https://rust-lang.github.io/rustfmt/
[speculoos]: https://github.com/oknozor/speculoos

[Shell]: https://en.wikipedia.org/wiki/Shell_%28computing%29
[Bash]: https://www.gnu.org/software/bash/
[shfmt]: https://github.com/mvdan/sh
[ShellCheck]: https://www.shellcheck.net
[shUnit2]: https://github.com/kward/shunit2

[Snapshot Testing]: https://en.wikipedia.org/wiki/Software_testing#Output_comparison_testing
[delta]: https://dandavison.github.io/delta/

[TypeScript]: https://www.typescriptlang.org/
[Bun]: https://bun.sh

[Deno]: https://deno.com

[NPM]: https://docs.npmjs.com/cli/
[Prettier]: https://prettier.io
[ts-node]: https://typestrong.org/ts-node/
[Jest]: https://jestjs.io

[vite-node]: https://github.com/vitest-dev/vitest/tree/main/packages/vite-node
[Vitest]: https://vitest.dev

### Others Bootstraps

List of alternatives bootstraps / setup / template with more languages

* [kata-bootstraps of Softwerkskammer Berlin](https://github.com/swkBerlin/kata-bootstraps)
* [CodeWorks' Katapult](https://github.com/CodeWorksFrance/katapult)
* [List of setup](https://github.com/orgs/cyber-dojo-start-points/repositories?type=source)
  * from [cyber-dojo](https://cyber-dojo.org/creator/home)
* [NixOS's templates](https://github.com/NixOS/templates)
* [DevEnv](https://devenv.sh/)
* [DevShell](https://numtide.github.io/devshell/)
* [DevBox](https://www.jetify.com/devbox/) which is used by this repository
* [Flox](https://floxdev.com/)
* More on [Awesome Nix](https://github.com/nix-community/awesome-nix#development)

## Do you like this project ?

<!-- markdownlint-disable-next-line MD039 MD045 -->
* If yes, please [add a star on GitLab ![](https://img.shields.io/gitlab/stars/pinage404/nix-sandboxes?style=social)](https://gitlab.com/pinage404/nix-sandboxes)
* If no, please [open an issue](https://gitlab.com/pinage404/nix-sandboxes/-/issues) to give your feedback

## Contributing

Any contributions ([feedback, bug report](https://gitlab.com/pinage404/nix-sandboxes/-/issues), [merge request](https://gitlab.com/pinage404/nix-sandboxes/-/merge_requests) ...) are welcome

### Adding a language

Follow [`CONTRIBUTING.md`](./CONTRIBUTING.md)

---

Fork of [`FaustXVI/sandboxes`](https://github.com/FaustXVI/sandboxes.git)
