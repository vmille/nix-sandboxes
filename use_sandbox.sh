#!/usr/bin/env bash

export NIX_CONFIG='extra-experimental-features = flakes nix-command'

DRY_RUN=''

use_sandbox() {
  SANDBOX="$1"
  DESTINATION_DIRECTORY="$2"

  if [[ "$DRY_RUN" == "" ]]; then
    set -o xtrace
  fi

  if command -v cachix; then
    $DRY_RUN cachix use pinage404-nix-sandboxes
    $DRY_RUN cachix use git-gamble
  fi
  $DRY_RUN nix flake new --template "gitlab:pinage404/nix-sandboxes#$SANDBOX" "$DESTINATION_DIRECTORY"
  $DRY_RUN direnv allow "$DESTINATION_DIRECTORY"
  $DRY_RUN cd "$DESTINATION_DIRECTORY" || exit 1
  if [[ "$EDITOR" != "" ]]; then
    # shellcheck disable=SC2086
    $DRY_RUN $EDITOR .
  fi
}

display_help() {
  BOLD=$(tput bold)
  NORMAL=$(tput sgr0)

  ENABLE_FLAKE="NIX_CONFIG=$NIX_CONFIG"

  echo "$BOLD## Usage$NORMAL"
  echo "$ENABLE_FLAKE \\"
  echo "nix run 'gitlab:pinage404/nix-sandboxes' -- [-n | --dry-run] [<SANDBOX> [<PATH>]]"
  echo ""
  echo "$BOLD### Exemple$NORMAL"
  echo "$ENABLE_FLAKE \\"
  echo "nix run 'gitlab:pinage404/nix-sandboxes' -- rust ./your_new_project_directory"
  echo ""
  echo "$BOLD## Sandboxes list$NORMAL"
  echo -e "$(nix run gitlab:pinage404/nix-sandboxes#sandboxes-list)"
}

SANDBOX=""
DESTINATION_DIRECTORY="${2:-./your_new_project_directory}"

SANDBOXES_NAME="$(nix run gitlab:pinage404/nix-sandboxes#sandboxes-name)"

for ARG in "$@"; do
  if [[ ":$SANDBOXES_NAME:" =~ :$ARG: ]]; then
    SANDBOX="$ARG"
  else
    case "$ARG" in
    -h | --help)
      display_help
      exit 0
      ;;

    -n | --dry-run)
      DRY_RUN='echo'
      ;;

    *)
      DESTINATION_DIRECTORY="$ARG"
      ;;
    esac
  fi
  shift
done

if [[ "$SANDBOX" = "" ]]; then
  display_help
  exit 1
fi

use_sandbox "$SANDBOX" "$DESTINATION_DIRECTORY"
