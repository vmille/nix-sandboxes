﻿namespace Service

module Say =
    open System

    let hello (name: option<String>) : String =
        name |> Option.defaultValue "world" |> (sprintf "Hello %s!")
