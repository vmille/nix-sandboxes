#ifndef HELLO_H
#define HELLO_H

#include <string>
#include <string_view>

std::string hello(std::string_view name);

#endif  /* !HELLO_H */
