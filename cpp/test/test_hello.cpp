#define CATCH_CONFIG_MAIN
#include "hello.h"
#include <catch2/catch.hpp>

TEST_CASE("test_hello_world") {
  auto const output = hello("");
  REQUIRE("Hello world" == output);
}

TEST_CASE("test_hello_foo") {
  auto const output = hello("foo");
  REQUIRE("Hello foo" == output);
}