(ns test-runner
  (:require
   [clojure.test :refer [run-tests]]))

(def modules-to-test
  '[hello-test])

(defn -main [& _args]
  (doall (map require modules-to-test))
  (let [test-result (apply run-tests modules-to-test)
        {:keys [fail error]} test-result
        exit-code (+ fail error)]
    (System/exit exit-code)))
