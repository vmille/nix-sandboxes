(ns hello
  (:require
   #_{:clj-kondo/ignore [:unused-referred-var :unused-namespace]}
   [debug.print :refer [pretty-print
                        spy
                        group-begin group-begin-collapsed group-end
                        print-group]]
   #_{:clj-kondo/ignore [:unused-referred-var :unused-namespace]}
   [debug.macro
    :refer [fn-input fn-output
            fn-io fn-group fn-group-io
            defn-io defn-group defn-group-io
            defn-generate-test]
    :refer-macros []]))

(defn hello
  [name]
  (str "Hello " (or name "world")))

(defn -main [& args]
  (println (hello (first args))))
